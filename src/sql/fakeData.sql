insert into "user"("id","first_name","last_name","birthdate","email","disabled","subscription_id")
            values (uuid_generate_v4(),'Matteo','Gorris','2000-11-14','matteo.gorris@gmail.com',false),
                   (uuid_generate_v4(),'Lucas','leclercq','2001-01-24','lucas.leclercq@gmail.com',false);

INSERT INTO "offer"("id","name","available","periodicity","pricing")
            values (uuid_generate_v4(),'Découverte',true,'2 months ago',19),
                   (uuid_generate_v4(),'Spécial Noel', true, '2 months ago',15);

INSERT INTO "subscription"("id","start_date","end_date","offer_id")
            values (uuid_generate_v4(),'2022-12-24','2023-12-24',
                    (SELECT "id" FROM "offer" WHERE name = 'Spécial Noel')),
                (uuid_generate_v4(),'2023-01-01','2023-01-03',
                (SELECT "id" FROM "offer" WHERE name = 'Découverte'));

insert into "paymentmethod" ("id","user_id")
            values (uuid_generate_v4(),(SELECT "id" FROM "user" WHERE "first_name" = 'Matteo'));

insert into "paypalpaymentmethod" ("id","account")
            values ((SELECT "id" FROM "paymentmethod"),'matteo.gorris@gmail.com');

insert into "cardpaymentmethod" ("id","name","number","expiration")
            values ((SELECT "id" FROM "paymentmethod"),'matteo','05250550','2025-02-25');