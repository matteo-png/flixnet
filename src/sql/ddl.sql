CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE "user"
(
    "id" UUID NOT NULL,
    "firstName" VARCHAR(255) NOT NULL,
    "lastName" VARCHAR(255) NOT NULL,
    "birthdate" date NOT NULL,
    "email" VARCHAR(255) NOT NULL,
    "disabled" BOOLEAN NOT NULL,
    "user_id" UUID NULL,

    PRIMARY KEY ("id"),
    foreign key ("user_id") references "subscription"("id"),
    UNIQUE ("email")
);
drop table "user";

create table "subscription"(
    "id" UUID NOT NULL,
    "start_date" date NOT NULL,
    "end_date" date NOT NULL,
    "offer_id" UUID NOT NULL,

    primary key ("id"),
    foreign key ("offer_id") references "offer"("id")
);

drop table "subscription";


create table "offer"(
    "id" UUID NOT NULL,
    "name" varchar(255) NOT NULL,
    "available" BOOLEAN NOT NULL,
    "periodicity" varchar(255) NOT NULL,
    "pricing" int NOT NULL,

    primary key ("id")
);

create table "paymentmethod"(
    "id" UUID NOT NULL,
    "user_id" UUID NULL,

    primary key ("id"),
    foreign key ("user_id") references "user"("id")
);


drop table "paymentmethod";

create table "paypalpaymentmethod"(
    "id" UUID NOT NULL,
    "account" varchar(255) NOT NULL,

    primary key ("id"),
    foreign key ("id") references "paymentmethod"("id")
);

drop table "paypalpaymentmethod";

create table "cardpaymentmethod"(
    "id" UUID NOT NULL,
    "name" varchar(255) NOT NULL,
    "number" varchar(255) NOT NULL,
    "expiration" date NOT NULL,

    primary key ("id"),
    foreign key ("id") references "paymentmethod"("id")
);

drop table "cardpaymentmethod";