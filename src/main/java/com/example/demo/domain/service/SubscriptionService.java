package com.example.demo.domain.service;

import com.example.demo.domain.model.Offer;
import com.example.demo.domain.model.Subscription;
import com.example.demo.domain.model.User;

import java.sql.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface SubscriptionService {

    List<Subscription> getAllSubscription();

    Optional<Subscription> getSubscription(UUID id);

    Subscription createSubscription(Date startDate, Date endDate, Offer offer);
}
