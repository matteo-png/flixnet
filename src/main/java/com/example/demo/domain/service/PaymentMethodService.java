package com.example.demo.domain.service;

import com.example.demo.domain.model.CardPaymentMethod;

import java.util.List;

public interface PaymentMethodService {
    List<CardPaymentMethod> getAllPayment();
}
