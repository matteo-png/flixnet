package com.example.demo.domain.service;

import com.example.demo.domain.model.Offer;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OfferService {
    List<Offer> getAllOffer();
    Optional<Offer> getOffer(UUID id);


    Offer createOffer(String name, boolean available, String periodicity, int pricing );
}
