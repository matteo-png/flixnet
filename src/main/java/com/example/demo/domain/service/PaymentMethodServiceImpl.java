package com.example.demo.domain.service;

import com.example.demo.data.repository.PaymentMethodRepository;
import com.example.demo.domain.model.CardPaymentMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentMethodServiceImpl {

    private final PaymentMethodRepository paymentMethodRepository;

    @Autowired
    public PaymentMethodServiceImpl(PaymentMethodRepository paymentMethodRepository){
        this.paymentMethodRepository = paymentMethodRepository;
    }


    public List<CardPaymentMethod> getAllPayment(){
        return paymentMethodRepository.findAll();
    }

}
