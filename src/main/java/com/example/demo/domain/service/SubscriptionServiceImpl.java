package com.example.demo.domain.service;

import com.example.demo.data.repository.SubscriptionRepository;
import com.example.demo.domain.event.SubscriptionCreatedEvent;
import com.example.demo.domain.model.Offer;
import com.example.demo.domain.model.Subscription;
import com.example.demo.domain.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class SubscriptionServiceImpl implements SubscriptionService {

    private final SubscriptionRepository subscriptionRepository;

    private final ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    public SubscriptionServiceImpl(SubscriptionRepository subscriptionRepository,ApplicationEventPublisher applicationEventPublisher){
        this.subscriptionRepository = subscriptionRepository;
        this.applicationEventPublisher =applicationEventPublisher;

    }

    @Override
    public List<Subscription> getAllSubscription() {
        return subscriptionRepository.findAll();
    }

    @Override
    public Optional<Subscription> getSubscription(UUID id){
        return subscriptionRepository.findById(id);
    }
    @Override
    @Transactional
    public Subscription createSubscription(Date startDate, Date endDate, Offer offer) {
        var subscription = new Subscription(startDate, endDate, offer);
        subscriptionRepository.save(subscription);
        applicationEventPublisher.publishEvent(new SubscriptionCreatedEvent(subscription));
        return subscription;
    }
}
