package com.example.demo.domain.service;

import com.example.demo.data.repository.UserRepository;
import com.example.demo.domain.event.UserCreatedEvent;
import com.example.demo.domain.model.Subscription;
import com.example.demo.domain.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, ApplicationEventPublisher applicationEventPublisher){
        this.userRepository = userRepository;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    public List<User> getAllUser() {
        return userRepository.findAll();
    }



@Override
    public Optional<User> getUser(UUID id){
        return userRepository.findById(id);
}

    @Override
    @Transactional
    public User createUser(String firstName, String lastName, Date birthdate, String email, boolean disabled, Subscription subscription) {
        var user = new User(firstName, lastName, birthdate, email,disabled, subscription);
        userRepository.save(user);
        applicationEventPublisher.publishEvent(new UserCreatedEvent(user));
        return user;
    }


}
