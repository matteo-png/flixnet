package com.example.demo.domain.service;

import com.example.demo.data.repository.OfferRepository;
import com.example.demo.domain.event.OfferCreatedEvent;
import com.example.demo.domain.model.Offer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class OfferServiceImpl implements OfferService{
    private final OfferRepository offerRepository;
    private final ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    public OfferServiceImpl(OfferRepository offerRepository, ApplicationEventPublisher applicationEventPublisher){
        this.offerRepository = offerRepository;
        this.applicationEventPublisher = applicationEventPublisher;
    }
    @Override
    public List<Offer> getAllOffer() {
        return offerRepository.findAll();
    }
    @Override
    public Optional<Offer> getOffer(UUID id){
        return offerRepository.findById(id);
    }

    @Override
    @Transactional
    public Offer createOffer(String name, boolean available, String periodicity, int pricing) {
        var offer = new Offer(name, available, periodicity, pricing);
        offerRepository.save(offer);
        applicationEventPublisher.publishEvent(new OfferCreatedEvent(offer));
        return offer;
    }
}
