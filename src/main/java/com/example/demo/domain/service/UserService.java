package com.example.demo.domain.service;

import com.example.demo.domain.model.Offer;
import com.example.demo.domain.model.Subscription;
import com.example.demo.domain.model.User;

import java.sql.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserService {

    List<User> getAllUser();

    Optional<User> getUser(UUID id);

    User createUser(String firstName, String lastName, Date birthdate, String email, boolean disabled, Subscription subscription);







}
