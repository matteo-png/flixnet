package com.example.demo.domain.event;

import com.example.demo.domain.model.Subscription;

public record SubscriptionCreatedEvent(Subscription subscription) {
}
