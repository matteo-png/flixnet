package com.example.demo.domain.event;

import com.example.demo.domain.model.Offer;

public record OfferCreatedEvent(Offer offer) {
}
