package com.example.demo.domain.event;

import com.example.demo.domain.model.User;

public record UserCreatedEvent(User user) {
}
