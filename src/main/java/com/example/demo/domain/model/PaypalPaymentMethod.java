package com.example.demo.domain.model;

import jakarta.persistence.*;

import java.util.UUID;

@Entity
@Table(name = "paypalpaymentmethod")
public class PaypalPaymentMethod extends PaymentMethod{

    @Column(name = "account")
    private String account;

    public PaypalPaymentMethod(String account){
        this.account = account;
    }

    protected PaypalPaymentMethod(){}


    public String getAccount() {
        return account;
    }

}
