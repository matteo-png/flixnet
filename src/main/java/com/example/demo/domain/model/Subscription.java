package com.example.demo.domain.model;

import jakarta.persistence.*;

import java.sql.Date;
import java.util.UUID;

@Entity
@Table(name = "subscription", schema = "public")
public class Subscription {
    @Id
    @GeneratedValue
    private UUID id;

    @Column(name="start_date")
    private Date startDate;

    @Column(name="end_date")
    private Date endDate;


    @OneToOne
    @JoinColumn(name="offer_id")
    private Offer offer;

    public Subscription(Date startDate, Date endDate, Offer offer) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.offer = offer;
    }

    protected Subscription(){

    }

    public UUID getId() {
        return id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }


    public Offer getOffer() {
        return offer;
    }

}
