package com.example.demo.domain.model;

import jakarta.persistence.*;


import java.sql.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

    @Entity
    @Table(name = "user", schema = "public")
    public class User {
        @Id
        @GeneratedValue
        private UUID id;
        @Column(name = "first_name")
        private String firstName;

        @Column(name="last_name")
        private String lastName;

        @Column(name="birthdate")
        private Date birthdate;

        @Column(name="email")
        private String email;

        @Column(name="disabled")
        private Boolean disabled;

        @OneToOne
        @JoinColumn(name="subscription_id")
        private Subscription subscription;

//        @OneToMany
//        @JoinColumn(name="user_id")
//        private Set<PaymentMethod> paymentMethods;

        public User(String firstName, String lastName, java.sql.Date birthdate, String email, Boolean disabled, Subscription subscription) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.birthdate = birthdate;
            this.email = email;
            this.disabled = disabled;
          //  this.paymentMethods = new HashSet<>();
            this.subscription = subscription;
        }
        protected User(){
        }

        public UUID getId() {
            return id;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public Date getBirthdate() {
            return birthdate;
        }

        public String getEmail() {
            return email;
        }

        public Boolean getDisabled() {
            return disabled;
        }

        public Subscription getSubscription() {
            return subscription;
        }

        public void setId(UUID id) {
            this.id = id;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public void setBirthdate(java.sql.Date birthdate) {
            this.birthdate = birthdate;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public void setDisabled(Boolean disabled) {
            this.disabled = disabled;
        }

        public void setSubscription(Subscription subscription) {
            this.subscription = subscription;
        }

//        public Set<PaymentMethod> getPaymentMethods() {
//            return Set.copyOf(paymentMethods);
//        }
//
//        public void addPaysmentMethod(PaymentMethod paymentMethod){
//            this.paymentMethods.add(paymentMethod);
//        }
    }


