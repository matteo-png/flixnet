package com.example.demo.domain.model;

import jakarta.persistence.*;

import java.util.UUID;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name="Paymentmethod", schema = "public")
public abstract class  PaymentMethod {

    @Id
    @GeneratedValue
    private UUID id;

    protected PaymentMethod(){}

    public UUID getId() {
        return id;
    }
}
