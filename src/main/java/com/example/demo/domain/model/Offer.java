package com.example.demo.domain.model;

import jakarta.persistence.*;

import java.sql.Date;
import java.util.UUID;

@Entity
@Table(name = "offer", schema = "public")
public class Offer {
    @Id
    @GeneratedValue
    private UUID id;

    @Column(name = "name")
    private String name;

    @Column(name="available")
    private boolean available;

    @Column(name="periodicity")
    private String periodicity;

    @Column(name="pricing")
    private int pricing;

    public Offer(String name, boolean available, String periodicity, int pricing) {
        this.name = name;
        this.available = available;
        this.periodicity = periodicity;
        this.pricing = pricing;
    }

    protected Offer(){

    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isAvailable() {
        return available;
    }

    public String getPeriodicity() {
        return periodicity;
    }

    public int getPricing() {
        return pricing;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public void setPeriodicity(String periodicity) {
        this.periodicity = periodicity;
    }

    public void setPricing(int pricing) {
        this.pricing = pricing;
    }
}
