package com.example.demo.domain.model;

import jakarta.persistence.*;

import java.sql.Date;
import java.util.UUID;

@Entity
@Table(name = "cardpaymentmethod")
public class CardPaymentMethod extends PaymentMethod{

    @Column(name = "name")
    private String name;

    @Column(name = "number")
    private String number;

    @Column(name = "expiration")
    private Date expiration;

    public CardPaymentMethod(String name, String number, Date expiration) {
        this.name = name;
        this.number = number;
        this.expiration = expiration;
    }

    protected CardPaymentMethod(){

    }


    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public Date getExpiration() {
        return expiration;
    }
}
