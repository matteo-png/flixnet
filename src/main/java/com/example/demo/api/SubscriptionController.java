package com.example.demo.api;

import com.example.demo.data.repository.UserRepository;
import com.example.demo.domain.model.Offer;
import com.example.demo.domain.model.Subscription;
import com.example.demo.domain.model.User;
import com.example.demo.domain.service.OfferService;
import com.example.demo.domain.service.SubscriptionService;
import com.example.demo.domain.service.UserService;
import com.example.demo.domain.service.UserServiceImpl;
import jakarta.persistence.Id;
import org.hibernate.annotations.UuidGenerator;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/subscription")
public class SubscriptionController {

    private final SubscriptionService subscriptionService;
    private final OfferService offerService;
    private final UserService userService;

    private final UserRepository userRepository;

public SubscriptionController(SubscriptionService subscriptionService, OfferService offerService, UserService userService, UserRepository userRepository){
    this.subscriptionService = subscriptionService;
    this.offerService = offerService;
    this.userService = userService;
    this.userRepository = userRepository;
}


@GetMapping
public List<Subscription> getAll(){
    return subscriptionService.getAllSubscription();
}

    @GetMapping("/{id}")
    public Subscription get(@PathVariable("id") UUID id) throws ChangeSetPersister.NotFoundException {
        return subscriptionService
                .getSubscription(id)
                .orElseThrow(ChangeSetPersister.NotFoundException::new);
    }

//    @GetMapping("/create")
//    public void createSubscription(){
//        UUID offerId = UUID.fromString("dd1f7850-0ed3-4581-8ef6-44bde84adef1");
//
//        Optional<Offer> offer1 = offerService.getOffer(offerId);
//
//        Offer offer = offer1.get();
//
//
//        if(offer.isAvailable() == true){
//            subscriptionService.createSubscription(Date.valueOf("2023-02-14"),Date.valueOf("2024-02-14"), offer );
//        }else{
//            System.out.println("l'abonnement n'a pas pu être crée car l'offre n'est plus valide");
//        }

   // }




}
