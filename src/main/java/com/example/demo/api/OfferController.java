package com.example.demo.api;

import com.example.demo.data.repository.OfferRepository;
import com.example.demo.domain.model.Offer;
import com.example.demo.domain.model.User;
import com.example.demo.domain.service.OfferService;
import com.example.demo.domain.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/offer")
public class OfferController {
    private final OfferService offerService;

    private final OfferRepository offerRepository;

    @Autowired
    public OfferController(OfferService offerService, OfferRepository offerRepository){
        this.offerService = offerService;
        this.offerRepository = offerRepository;
    }

    @GetMapping
    public List<Offer> getAll(){
        return offerService.getAllOffer();
    }

    @GetMapping("/{id}")
    public Offer get(@PathVariable("id") UUID id) throws ChangeSetPersister.NotFoundException {
        return offerService
                .getOffer(id)
                .orElseThrow(ChangeSetPersister.NotFoundException::new);
    }

    @GetMapping("/create")
    public void createOffer(){
        offerService.createOffer("toto",true, "1 years",20);
    }

    @GetMapping ("/disabled/{id}")
    public void DisabledOffer(@PathVariable("id") UUID id) {
        Optional<Offer> offerData = offerService.getOffer(id);

        Offer offer = offerData.get();
        offer.setAvailable(false);

        offerRepository.save(offer);

//        final Offer updateOffer = offerRepository.save(offer);
//        return ResponseEntity.ok(updateOffer).getBody();

    }

}
