package com.example.demo.api;


import com.example.demo.data.repository.UserRepository;
import com.example.demo.domain.model.Offer;
import com.example.demo.domain.model.Subscription;
import com.example.demo.domain.model.User;
import com.example.demo.domain.service.OfferService;
import com.example.demo.domain.service.SubscriptionService;
import com.example.demo.domain.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    private final UserRepository userRepository;

    private final SubscriptionService subscriptionService;
    private final OfferService offerService;

    @Autowired
    public UserController(UserService userService, UserRepository userRepository, SubscriptionService subscriptionService, OfferService offerService){
        this.userService = userService;
        this.userRepository = userRepository;
        this.subscriptionService = subscriptionService;
        this.offerService = offerService;
    }

    @GetMapping
    public List<User> getAll(){
        return userService.getAllUser();
    }

    @GetMapping("/{id}")
    public User get(@PathVariable("id") UUID id) throws ChangeSetPersister.NotFoundException {
        return userService
                .getUser(id)
                .orElseThrow(ChangeSetPersister.NotFoundException::new);
    }

    @GetMapping("/create")
    public void createUser(){
        userService.createUser("test","test", Date.valueOf("2006-11-14"),"test.test@gmail.com",false, null);

    }


    @GetMapping("/subscription/{id}")
    public void SubscriptionUser(@PathVariable("id") UUID id){
        Optional<User> userData = userService.getUser(id);
        User user = userData.get();

        UUID subscriptionID = UUID.fromString("218734dc-6886-4758-b0c9-7fcee0b8a4c0");

        Optional<Subscription> subscription1 = subscriptionService.getSubscription(subscriptionID);
        Subscription subscription = subscription1.get();


        UUID offerId = UUID.fromString("dd1f7850-0ed3-4581-8ef6-44bde84adef1");

        Optional<Offer> offer1 = offerService.getOffer(offerId);

        Offer offer = offer1.get();



        if(user.getDisabled() == true){
            System.out.println("l'user n'a pas pu s'abonner car il est désactiver");
        }else if(offer.isAvailable() == true){
            subscriptionService.createSubscription(Date.valueOf("2023-02-14"),Date.valueOf("2024-02-14"), offer );
            user.setSubscription(subscription);
            userRepository.save(user);
        }else{
            System.out.println("l'abonnement n'a pas pu être crée car l'offre n'est plus valide");

        }


    }

    @GetMapping ("/disabled/{id}")
    public void DisabledUser(@PathVariable("id") UUID id) {
        Optional<User> userData = userService.getUser(id);

        User user = userData.get();
        user.setDisabled(true);
        if(user.getDisabled()== true){
            user.setSubscription(null);
        }
            //final User disabledUser = userRepository.save(user);
        userRepository.save(user);

      //  return ResponseEntity.ok(disabledUser).getBody();

    }




}

